package com.bloomberg.util;

import com.bloomberg.dto.BaseDealsDto;
import com.bloomberg.entity.InvalidDeals;
import com.bloomberg.entity.SourceDeals;
import com.bloomberg.entity.ValidDeals;

public class ConvertUtil {

    public static ValidDeals convertBaseDealDtoToValidDeals(BaseDealsDto baseDealsDto, SourceDeals sourceDeals) {
        ValidDeals validDeals = new ValidDeals();
        validDeals.setUniqueId(baseDealsDto.getUniqueId());
        validDeals.setFileName(sourceDeals.getFileName());
        validDeals.setAmount(baseDealsDto.getAmount());
        validDeals.setFromCurrencyIsoCode(baseDealsDto.getFromCurrencyIsoCode());
        validDeals.setToCurrencyIsoCode(baseDealsDto.getToCurrencyIsoCode());
        validDeals.setSourceDeals(sourceDeals);

        return validDeals;
    }

    public static InvalidDeals convertBaseDealDtoToInvalidDeals(BaseDealsDto baseDealsDto, SourceDeals sourceDeals) {
        InvalidDeals invalidDeals = new InvalidDeals();

        invalidDeals.setUniqueId(baseDealsDto.getUniqueId());
        invalidDeals.setFileName(sourceDeals.getFileName());
        invalidDeals.setAmount(baseDealsDto.getAmount());
        invalidDeals.setFromCurrencyIsoCode(baseDealsDto.getFromCurrencyIsoCode());
        invalidDeals.setToCurrencyIsoCode(baseDealsDto.getToCurrencyIsoCode());
        invalidDeals.setSourceDeals(sourceDeals);

        return invalidDeals;
    }
}
