package com.bloomberg.util;

import com.bloomberg.dto.BaseDealsDto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class FileGeneratorUtil {

    private static String filePath;

    static SimpleDateFormat TIMESTAMP_FORMAT = new SimpleDateFormat("yyyyMMddHHmmss");
    static SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");


    public static String generateSampleFile() {

        String fileSample = filePath + File.separator + "deals" + TIMESTAMP_FORMAT.format(new Date()) + ".csv";

        List<BaseDealsDto> deals = new ArrayList<>();

        for (int a = 1; a <= 100000; a++) {
            if (a % 2 == 0) {
                generateValidData(deals);
            } else {
                generateInvalidData(deals);

            }
        }

        writeToCSVFile(fileSample, deals);


        return "Sample file generated successfully at location " + fileSample;

    }

    private static void writeToCSVFile(String fileSample, List<BaseDealsDto> deals) {
        FileWriterUtil fileWriterUtils = new FileWriterUtil();
        fileWriterUtils.writeDataToCSV(fileSample, deals);
    }

    private static void generateInvalidData(List<BaseDealsDto> deals) {
        BaseDealsDto invalidDeal = new BaseDealsDto();
        invalidDeal.setUniqueId(UniqueIdGeneratorUtil.getRandomGeneratedUniqueIdForSampleFile());
        invalidDeal.setFromCurrencyIsoCode(CurrencyCodeGeneratorUtil.getRandomInvalidCurrencyCode());
        invalidDeal.setToCurrencyIsoCode(CurrencyCodeGeneratorUtil.getRandomValidCurrencyCode());
        invalidDeal.setAmount(AmountGeneratorUtil.getInValidAmount());
        invalidDeal.setDealStartDate(getTimeStamp());
        deals.add(invalidDeal);
    }

    private static void generateValidData(List<BaseDealsDto> deals) {
        BaseDealsDto validDeal = new BaseDealsDto();
        validDeal.setUniqueId(UniqueIdGeneratorUtil.getRandomGeneratedUniqueIdForSampleFile());
        validDeal.setFromCurrencyIsoCode(CurrencyCodeGeneratorUtil.getRandomValidCurrencyCode());
        validDeal.setToCurrencyIsoCode(CurrencyCodeGeneratorUtil.getRandomValidCurrencyCode());
        validDeal.setAmount(AmountGeneratorUtil.getValidAmount());
        validDeal.setDealStartDate(getTimeStamp());
        deals.add(validDeal);
    }

    public static String getTimeStamp() {
        return DATE_FORMAT.format(new Date());
    }

    public String getFilePath() {
        return filePath;
    }

    @Value("${app.filepath.sample-location}")
    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }


}
