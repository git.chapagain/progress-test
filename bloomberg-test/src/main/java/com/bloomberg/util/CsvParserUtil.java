package com.bloomberg.util;

import com.bloomberg.dto.BaseDealsDto;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Created by tchapagai on 12/03/2018.
 */
public class CsvParserUtil {

    public static List<BaseDealsDto> getDatasFromCsvFile(String filePath) throws IOException {

        try (BufferedReader reader = new BufferedReader(new FileReader(filePath))) {
            return reader.lines().skip(1)
                    .map(mapToBaseDeals)
                    .collect(Collectors.toList());
        } catch (IOException fe) {
            fe.printStackTrace();
            throw fe;
        }
    }

    private static Function<String, BaseDealsDto> mapToBaseDeals = line -> {
        BaseDealsDto deal = new BaseDealsDto();

        String[] dealsCSV = line.split(",");
        IntStream.range(0, dealsCSV.length).forEach(index -> {
            switch (index) {

                case 0: {
                    deal.setUniqueId(dealsCSV[0]);
                    break;
                }
                case 1: {
                    deal.setFromCurrencyIsoCode(dealsCSV[1]);
                    break;

                }
                case 2: {
                    deal.setToCurrencyIsoCode(dealsCSV[2]);
                    break;
                }
                case 3: {
                    deal.setDealStartDate(dealsCSV[3]);
                    break;
                }
                case 4: {
                    deal.setAmount(Double.valueOf(dealsCSV[4]));
                    break;
                }
                default:
                    break;
            }
        });


        return deal;
    };

    public static String getFileExtension(MultipartFile file) {
        String name = file.getOriginalFilename();
        try {
            return name.substring(name.lastIndexOf(".") + 1);
        } catch (Exception e) {
            return "";
        }
    }

}
