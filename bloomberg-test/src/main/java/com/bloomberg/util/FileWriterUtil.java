package com.bloomberg.util;

import com.bloomberg.dto.BaseDealsDto;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;


public class FileWriterUtil {

    public void writeDataToCSV(String fileName, List<BaseDealsDto> deals) {

        try (FileWriter writer = new FileWriter(fileName)) {
            writer.write(createHeader());
            for (BaseDealsDto deal : deals) {
                writer.write(deal.toCSV());
            }

        } catch (IOException e) {
//            log.error(e.getMessage());
        }
    }

    public String createHeader() {

        String UNIQUE_ID = "UNIQUE ID";
        String FROM_CURRENCY_CODE = "FROM_CURRENCY_CODE";
        String TO_CURRENCY_CODE = "TO_CURRENCY_CODE";
        String DEAL_START_DATE = "DEAL_START_DATE";
        String DEAL_AMOUNT = "DEAL_AMOUNT";

        return UNIQUE_ID + "\t" + FROM_CURRENCY_CODE + "\t" + TO_CURRENCY_CODE + "\t" + DEAL_START_DATE + "\t" + DEAL_AMOUNT + "\r\n";


    }
}
