package com.bloomberg.util;

import java.util.UUID;

public class UniqueIdGeneratorUtil {

    public static String getRandomGeneratedUniqueIdForSampleFile() {
        return UUID.randomUUID().toString();
    }
}
