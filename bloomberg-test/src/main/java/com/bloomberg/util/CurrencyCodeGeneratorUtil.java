package com.bloomberg.util;

import java.util.Random;

public class CurrencyCodeGeneratorUtil {

    private static final String[] VALID_COUNTRY_CODE = new String[]{"NPR", "AUS", "GBP", "CAD", "USA", "AED"};

    private static final String[] INVALID_COUNTRY_CODE = new String[]{"ZZZ", "CCC", "QQQ", "TTT", "PLZ", "KQQ"};

    public static String getRandomValidCurrencyCode() {
        return VALID_COUNTRY_CODE[new Random().nextInt(VALID_COUNTRY_CODE.length)];
    }

    public static String getRandomInvalidCurrencyCode() {
        return INVALID_COUNTRY_CODE[new Random().nextInt(INVALID_COUNTRY_CODE.length)];
    }
}
