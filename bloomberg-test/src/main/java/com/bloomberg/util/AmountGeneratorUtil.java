package com.bloomberg.util;

import java.util.Random;

public class AmountGeneratorUtil {

    private static final double MIN = 10;
    private static final double MAX = 1000;

    public static double getValidAmount() {
        return Math.round(MIN + (MAX - MIN) * new Random().nextDouble() * 100d) / 100d;
    }


    public static double getInValidAmount() {
        return Math.round(MIN + (MIN - MAX) * new Random().nextDouble() * 100d) / 100d;
    }
}
