package com.bloomberg.thread;

import com.bloomberg.dao.InvalidDealDao;
import com.bloomberg.dao.ValidDealDao;
import com.bloomberg.entity.InvalidDeals;
import com.bloomberg.entity.ValidDeals;

import java.util.List;

public class BatchProcessor implements Runnable {

    private ValidDealDao validDealDao;

    private InvalidDealDao invalidDealDao;

    private List<ValidDeals> validBatch;

    private List<InvalidDeals> invalidBatch;


    public BatchProcessor(List<ValidDeals> validBatch, ValidDealDao validDealDao) {
        this.validBatch = validBatch;
        this.validDealDao = validDealDao;
    }

    public BatchProcessor(List<InvalidDeals> invalidBatch, InvalidDealDao invalidDealDao) {
        this.invalidBatch = invalidBatch;
        this.invalidDealDao = invalidDealDao;
    }

    @Override
    public void run() {
        try {

            if (validDealDao != null) {
                validDealDao.save(validBatch);
            }
            if (invalidDealDao != null) {
                invalidDealDao.save(invalidBatch);
            }

            Thread.interrupted();
        } catch (Exception e) {
            e.printStackTrace();
            try {
                Thread.sleep(5000);
            } catch (InterruptedException ex) {
            }

        }

    }
}
