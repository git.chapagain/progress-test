package com.bloomberg.entity;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class BaseDeal extends AbstractBaseEntity{

    @Column(name = "unique_id")
    private String uniqueId;

    @Column(name = "from_currency_iso_code")
    private String fromCurrencyIsoCode;

    @Column(name = "to_currency_iso_code")
    private String toCurrencyIsoCode;

    @Column(name = "file_name")
    private String fileName;

    @Column(name = "amount")
    private Double amount;

    @JoinColumn(name = "SOURCE_DEALS")
    @ManyToOne
    private SourceDeals sourceDeals;

    public String getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getFromCurrencyIsoCode() {
        return fromCurrencyIsoCode;
    }

    public void setFromCurrencyIsoCode(String fromCurrencyIsoCode) {
        this.fromCurrencyIsoCode = fromCurrencyIsoCode;
    }

    public String getToCurrencyIsoCode() {
        return toCurrencyIsoCode;
    }

    public void setToCurrencyIsoCode(String toCurrencyIsoCode) {
        this.toCurrencyIsoCode = toCurrencyIsoCode;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public SourceDeals getSourceDeals() {
        return sourceDeals;
    }

    public void setSourceDeals(SourceDeals sourceDeals) {
        this.sourceDeals = sourceDeals;
    }
}
