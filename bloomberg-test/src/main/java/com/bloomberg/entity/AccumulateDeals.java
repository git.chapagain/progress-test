/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bloomberg.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author sds
 */
@Entity
@Table(name = "ACCUMULATE_DEALS")
public class AccumulateDeals implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "ISO_CURRENCY_CODE")
    private String isoCurrencyCode;

    @Column(name = "DEAL_COUNT")
    private int dealCount;

    @JoinColumn(name = "SOURCE_DEALS")
    @ManyToOne
    private SourceDeals sourceDeals;

    public AccumulateDeals() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIsoCurrencyCode() {
        return isoCurrencyCode;
    }

    public void setIsoCurrencyCode(String isoCurrencyCode) {
        this.isoCurrencyCode = isoCurrencyCode;
    }

    public int getDealCount() {
        return dealCount;
    }

    public void setDealCount(int dealCount) {
        this.dealCount = dealCount;
    }

    public SourceDeals getSourceDeals() {
        return sourceDeals;
    }

    public void setSourceDeals(SourceDeals sourceDeals) {
        this.sourceDeals = sourceDeals;
    }

}
