package com.bloomberg.service;


import com.bloomberg.dao.AccumulativeDealDao;
import com.bloomberg.entity.AccumulateDeals;
import com.bloomberg.entity.SourceDeals;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * Created by tchapagai on 12/03/2018.
 */
@Service
public class AccumulativeDealService {

    private final AccumulativeDealDao accumulativeDealDao;

    @Autowired
    public AccumulativeDealService(AccumulativeDealDao accumulativeDealDao) {
        this.accumulativeDealDao = accumulativeDealDao;
    }

    public void saveNewAndUpdateExistingAccumulativeDeals(Map<String, Long> countOfCurrency, SourceDeals sourceDeals) {
        for (Map.Entry<String, Long> entry : countOfCurrency.entrySet()) {
            AccumulateDeals accumulateDeal = getAccumulateDeal(entry, sourceDeals);
            if (accumulateDeal != null) {
                setCurrencyIsoCodeAndDealsCount(entry, accumulateDeal, sourceDeals);
            } else {
                accumulateDeal = new AccumulateDeals();
                setCurrencyIsoCodeAndDealsCount(entry, accumulateDeal, sourceDeals);

            }
            accumulativeDealDao.save(accumulateDeal);
        }
    }

    private void setCurrencyIsoCodeAndDealsCount(Map.Entry<String, Long> entry, AccumulateDeals accumulateDeals, SourceDeals sourceDeals) {
        accumulateDeals.setIsoCurrencyCode(entry.getKey());
        accumulateDeals.setDealCount(entry.getValue().intValue());
        accumulateDeals.setSourceDeals(sourceDeals);
    }

    private AccumulateDeals getAccumulateDeal(Map.Entry<String, Long> entry, SourceDeals sourceDeals) {
        return accumulativeDealDao.getAccumulateDealsByIsoCurrencyCodeAndSourceDeals(entry.getKey(), sourceDeals);
    }
}
