package com.bloomberg.service;

import com.bloomberg.dao.InvalidDealDao;
import com.bloomberg.dao.SourceDealDao;
import com.bloomberg.dao.ValidDealDao;
import com.bloomberg.dto.UploadedDealsFileDto;
import com.bloomberg.entity.SourceDeals;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class FileUploadService {

    private final SourceDealDao sourceDealDao;
    private final ValidDealDao validDealDao;
    private final InvalidDealDao invalidDealDao;

    @Autowired
    public FileUploadService(SourceDealDao sourceDealDao, ValidDealDao validDealDao, InvalidDealDao invalidDealDao) {
        this.sourceDealDao = sourceDealDao;
        this.validDealDao = validDealDao;
        this.invalidDealDao = invalidDealDao;
    }

    public List<UploadedDealsFileDto> getUploadedFiles() {

        List<SourceDeals> sourceDeals = sourceDealDao.findAll();
        List<UploadedDealsFileDto> totalUploadedDealFileDto = new ArrayList<>();

        for (SourceDeals deal : sourceDeals) {

            UploadedDealsFileDto uploadedDealsFileDto = new UploadedDealsFileDto();
            long validUploadedDealsCount = validDealDao.getValidDealCount(deal.getFileName());
            long invalidUploadedDealsCount = invalidDealDao.getInvalidDealCount(deal.getFileName());

            uploadedDealsFileDto.setFileName(deal.getFileName());
            uploadedDealsFileDto.setDealStartDate(deal.getStartDate());
            uploadedDealsFileDto.setDealEndDate(deal.getEndDate());
            uploadedDealsFileDto.setValidDealsCount(validUploadedDealsCount);
            uploadedDealsFileDto.setInvalidDealsCount(invalidUploadedDealsCount);
            uploadedDealsFileDto.setProcessedTime((deal.getEndDate().getTime() - deal.getStartDate().getTime()) / 1000 % 60);
            totalUploadedDealFileDto.add(uploadedDealsFileDto);

        }
        return totalUploadedDealFileDto;

    }

}

