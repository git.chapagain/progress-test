package com.bloomberg.service;

import com.bloomberg.dao.InvalidDealDao;
import com.bloomberg.dao.SourceDealDao;
import com.bloomberg.dao.ValidDealDao;
import com.bloomberg.dto.BaseDealsDto;
import com.bloomberg.entity.InvalidDeals;
import com.bloomberg.entity.SourceDeals;
import com.bloomberg.entity.ValidDeals;
import com.bloomberg.thread.BatchProcessor;
import com.bloomberg.util.ConvertUtil;
import com.bloomberg.util.CsvParserUtil;
import com.bloomberg.util.ValidationUtil;
import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

@Service
@PropertySource("classpath:application.properties")
@Configuration
public class DealProcessorService {

    Logger logger = LoggerFactory.getLogger(DealProcessorService.class);

    private static final String CSV = "csv";

    private final SourceDealDao sourceDealDao;
    private final ValidDealDao validDealDao;
    private final InvalidDealDao invalidDealDao;
    private final AccumulativeDealService accumulativeDealService;

    private String uploadedFilePath;

    @Value("${app.filepath.upload-location}")
    public void setUploadedFilePath(String uploadedFilePath) {
        this.uploadedFilePath = uploadedFilePath;
    }

    @Autowired
    public DealProcessorService(SourceDealDao sourceDealDao, ValidDealDao validDealDao, InvalidDealDao invalidDealDao, AccumulativeDealService accumulativeDealService) {
        this.sourceDealDao = sourceDealDao;
        this.validDealDao = validDealDao;
        this.invalidDealDao = invalidDealDao;
        this.accumulativeDealService = accumulativeDealService;
    }

    public String processFileUpload(MultipartFile file) {
        String message = "";

        try {
            String fileExtension = getUploadedFileExtension(file);


            if (isValidFileExtension(fileExtension)) {
                message = "Please upload file with .csv extension.";
                return message;
            }

            if (checkFileIsAlreadyUploaded(file)) {
                message = "File already exists";
                return message;
            }

            //parse a CSV file and return deals
            logger.info(file.getOriginalFilename() + " lading");
            List<BaseDealsDto> baseDealsDtos = loadDealFromCsvFile(file);

            //save a uploaded csv file
            SourceDeals sourceDeal = saveNewDeal(file);

            //get list of valid and invalid deal from the uploaded csv file
            List<ValidDeals> validDeals = getValidDeals(baseDealsDtos, sourceDeal);
            List<InvalidDeals> inValidDeals = getInvalidDeals(baseDealsDtos, sourceDeal);

            //group valid and invalid callables for execution
            createValidDealBatchAndSave(validDeals);
            createInvalidDealBatchAndSave(inValidDeals);


            //set deal end date and save
            updateDealWithEndDate(file.getOriginalFilename());

            //accumulate currency code
            accumulateCurrencyCode(validDeals, sourceDeal);

            message = "File uploaded successfully.";

        } catch (Exception e) {
            message = "File upload failed!!";
            logger.error(e.getMessage());
        }

        return message;

    }

    private void accumulateCurrencyCode(List<ValidDeals> validDeals, SourceDeals sourceDeals) {
        accumulativeDealService.saveNewAndUpdateExistingAccumulativeDeals(countByFromCurrencyCode(validDeals), sourceDeals);
    }

    public Map<String, Long> countByFromCurrencyCode(List<ValidDeals> validDeals) {
        return validDeals.stream().collect(Collectors.groupingBy(ValidDeals::getFromCurrencyIsoCode, Collectors.counting()));
    }


    private void createValidDealBatchAndSave(List<ValidDeals> validDeals) {

        ExecutorService validService = Executors.newFixedThreadPool(20);
        Iterable<List<ValidDeals>> validSets = Lists.partition(validDeals, 1000);

        for(List<ValidDeals> validBatch: validSets){
            validService.submit(new BatchProcessor(validBatch, validDealDao));
        }
        validService.shutdown();
    }

    private void createInvalidDealBatchAndSave(List<InvalidDeals> invalidDeals) {

        ExecutorService inValidService = Executors.newFixedThreadPool(20);
        Iterable<List<InvalidDeals>> invalidSets = Lists.partition(invalidDeals, 1000);

        for(List<InvalidDeals> invalidBatch: invalidSets){
            inValidService.submit(new BatchProcessor(invalidBatch, invalidDealDao));
        }
        inValidService.shutdown();
    }


    private SourceDeals saveNewDeal(MultipartFile file) {
        SourceDeals sourceDeal = new SourceDeals();
        sourceDeal.setFileName(file.getOriginalFilename());
        sourceDeal.setStartDate(new Date());
        return sourceDealDao.save(sourceDeal);
    }

    private void updateDealWithEndDate(String fileName) {
        SourceDeals sourceDeal = sourceDealDao.getSourceDealsByFileName(fileName);

        if (sourceDeal != null) {
            sourceDeal.setEndDate(new Date());
            sourceDealDao.save(sourceDeal);
        }
        logger.info("Saved file Date::" + new Date());
    }


    private List<ValidDeals> getValidDeals(List<BaseDealsDto> baseDealsDtos, SourceDeals deal) {
        return baseDealsDtos.stream().filter(baseDealsDto -> ValidationUtil.isDealValid(baseDealsDto))
                .map(baseDealsDto -> ConvertUtil.convertBaseDealDtoToValidDeals(baseDealsDto, deal))
                .collect(Collectors.toList());
    }

    private List<InvalidDeals> getInvalidDeals(List<BaseDealsDto> baseDealsDtos, SourceDeals deal) {
        return baseDealsDtos.stream().filter(baseDealsDto -> !ValidationUtil.isDealValid(baseDealsDto))
                .map(baseDealsDto -> ConvertUtil.convertBaseDealDtoToInvalidDeals(baseDealsDto, deal))
                .collect(Collectors.toList());
    }


    private boolean checkFileIsAlreadyUploaded(MultipartFile file) {
        return sourceDealDao.getSourceDealsByFileName(file.getOriginalFilename()) != null;
    }

    private boolean isValidFileExtension(String fileExtension) {
        return !CSV.equalsIgnoreCase(fileExtension);

    }

    private List<BaseDealsDto> loadDealFromCsvFile(MultipartFile file) throws IOException {
        return CsvParserUtil.getDatasFromCsvFile(writeUpLoadedFileAndGetFilePath(file).toString());

    }

    private String getUploadedFileExtension(MultipartFile file) {
        return CsvParserUtil.getFileExtension(file);
    }

    private Path writeUpLoadedFileAndGetFilePath(MultipartFile file) throws IOException {
        byte[] fileByteArray = file.getBytes();
        Path filePath = Paths.get(uploadedFilePath + file.getOriginalFilename());
        Files.write(filePath, fileByteArray);
        return filePath;
    }
}
