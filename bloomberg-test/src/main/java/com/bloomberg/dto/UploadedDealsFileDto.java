package com.bloomberg.dto;

import java.util.Date;

public class UploadedDealsFileDto {

    private String fileName;

    private Long validDealsCount;

    private Long invalidDealsCount;

    private Date dealStartDate;

    private Date dealEndDate;

    private Long processedTime;

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Long getValidDealsCount() {
        return validDealsCount;
    }

    public void setValidDealsCount(Long validDealsCount) {
        this.validDealsCount = validDealsCount;
    }

    public Long getInvalidDealsCount() {
        return invalidDealsCount;
    }

    public void setInvalidDealsCount(Long invalidDealsCount) {
        this.invalidDealsCount = invalidDealsCount;
    }

    public Date getDealStartDate() {
        return dealStartDate;
    }

    public void setDealStartDate(Date dealStartDate) {
        this.dealStartDate = dealStartDate;
    }

    public Date getDealEndDate() {
        return dealEndDate;
    }

    public void setDealEndDate(Date dealEndDate) {
        this.dealEndDate = dealEndDate;
    }

    public Long getProcessedTime() {
        return processedTime;
    }

    public void setProcessedTime(Long processedTime) {
        this.processedTime = processedTime;
    }
}
