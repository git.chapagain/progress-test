package com.bloomberg.dto;


/**
 * Created by tchapagai on 12/03/2018.
 */
public class BaseDealsDto {

    private String uniqueId;

    private String fromCurrencyIsoCode;

    private String toCurrencyIsoCode;

    private String dealStartDate;

    private Double amount;

    public String getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getFromCurrencyIsoCode() {
        return fromCurrencyIsoCode;
    }

    public void setFromCurrencyIsoCode(String fromCurrencyIsoCode) {
        this.fromCurrencyIsoCode = fromCurrencyIsoCode;
    }

    public String getToCurrencyIsoCode() {
        return toCurrencyIsoCode;
    }

    public void setToCurrencyIsoCode(String toCurrencyIsoCode) {
        this.toCurrencyIsoCode = toCurrencyIsoCode;
    }

    public String getDealStartDate() {
        return dealStartDate;
    }

    public void setDealStartDate(String dealStartDate) {
        this.dealStartDate = dealStartDate;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public BaseDealsDto() {

    }

    public BaseDealsDto(String uniqueId, String fromCurrencyIsoCode, String toCurrencyIsoCode, String dealStartDate, Double amount) {
        this.uniqueId = uniqueId;
        this.fromCurrencyIsoCode = fromCurrencyIsoCode;
        this.toCurrencyIsoCode = toCurrencyIsoCode;
        this.dealStartDate = dealStartDate;
        this.amount = amount;
    }

    public String toCSV() {
        return this.uniqueId + "," + this.fromCurrencyIsoCode + "," + this.toCurrencyIsoCode + "," + this.dealStartDate + "," + this.amount + "\r\n";
    }
}

