package com.bloomberg.controller;

import com.bloomberg.dto.UploadedDealsFileDto;
import com.bloomberg.service.DealProcessorService;
import com.bloomberg.service.FileUploadService;
import com.bloomberg.util.FileGeneratorUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * Created by tchapagai on 12/03/2018.
 */
@Controller
public class MainPageController {

    Logger log = LoggerFactory.getLogger(MainPageController.class);

    private final FileUploadService fileUploadService;
    private final DealProcessorService dealProcessorService;

    @Autowired
    public MainPageController(FileUploadService fileUploadService, DealProcessorService dealProcessorService) {
        this.fileUploadService = fileUploadService;
        this.dealProcessorService = dealProcessorService;
    }


    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String getUploadedDealsFile(ModelMap modelMap) {
        List<UploadedDealsFileDto> uploadedDealsFileDtos = fileUploadService.getUploadedFiles();
        modelMap.put("uploadedFiles", uploadedDealsFileDtos);
        return "index";
    }


    @RequestMapping(method = RequestMethod.POST , value = "/generateSampleFile")
    public String generateSampleFile(ModelMap modelMap){
        try {
//            log.debug("Processing to generate a file");
            String message = FileGeneratorUtil.generateSampleFile();
            modelMap.put("result", message);
            List<UploadedDealsFileDto> uploadedDealsFile = fileUploadService.getUploadedFiles();
            modelMap.put("uploadedFiles", uploadedDealsFile);
        }catch (Exception e){
            modelMap.put("result", "File cannot be generated at the moment");
        }
//        log.debug("Sent message {} ", modelMap.get("result"));
        return "index";
    }


    @RequestMapping(value = "/uploadFile", method = RequestMethod.POST)
    public String uploadFile(@RequestParam("file") MultipartFile file, ModelMap modelMap) {

        if (file.getOriginalFilename().isEmpty()) {
            List<UploadedDealsFileDto> uploadedFiles = fileUploadService.getUploadedFiles();
            modelMap.put("uploadedFiles", uploadedFiles);
            modelMap.put("fileUploadMessage", "No file uploaded!!!");
            return "index";
        }

        try {
            String fileUploadMessage = dealProcessorService.processFileUpload(file);
            modelMap.put("fileUploadMessage", fileUploadMessage);
            List<UploadedDealsFileDto> uploadedFiles = fileUploadService.getUploadedFiles();
            modelMap.put("uploadedFiles", uploadedFiles);
        } catch (Exception e) {
            log.error(e.getMessage());
        }

        return "index";
    }

}
