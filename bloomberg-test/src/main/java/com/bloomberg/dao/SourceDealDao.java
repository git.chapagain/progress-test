/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bloomberg.dao;


import com.bloomberg.entity.SourceDeals;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * @author tchapagain
 */

@Repository
public interface SourceDealDao extends JpaRepository<SourceDeals, Long> {

    @Query("select d from SourceDeals d where d.fileName =:fileName")
    SourceDeals getSourceDealsByFileName(@Param("fileName") String fileName);

}
