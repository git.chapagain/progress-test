package com.bloomberg.dao;

import com.bloomberg.entity.ValidDeals;

import java.util.List;

public interface ValidDealDao {

    public void save(List<ValidDeals> validDeals);

    public List<ValidDeals> getValidDeals(String fileName);

    public long getValidDealCount(String fileName);
}
