package com.bloomberg.dao;

import com.bloomberg.entity.InvalidDeals;

import java.util.List;

public interface InvalidDealDao {

    public void save(List<InvalidDeals> invalidDeals);

    public List<InvalidDeals> getInvalidDeals(String fileName);

    public long getInvalidDealCount(String fileName);
}
