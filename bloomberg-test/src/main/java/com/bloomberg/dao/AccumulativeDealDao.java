package com.bloomberg.dao;

import com.bloomberg.entity.AccumulateDeals;
import com.bloomberg.entity.SourceDeals;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AccumulativeDealDao extends JpaRepository<AccumulateDeals, Long> {

    @Query("SELECT ac FROM AccumulateDeals ac GROUP BY ac.isoCurrencyCode")
    List<AccumulateDeals> getAccumulateDealsGroupByIsoCurrencyCode();

    @Query("SELECT ac from AccumulateDeals ac where ac.isoCurrencyCode = :isoCurrencyCode AND ac.sourceDeals=:sourceDeals")
    AccumulateDeals getAccumulateDealsByIsoCurrencyCodeAndSourceDeals(@Param("isoCurrencyCode") String isoCurrencyCode, @Param("sourceDeals")SourceDeals sourceDeals);
}
