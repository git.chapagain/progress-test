package com.bloomberg.dao.impl;

import com.bloomberg.dao.InvalidDealDao;
import com.bloomberg.entity.InvalidDeals;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceUnit;
import javax.transaction.Transactional;
import java.util.List;

@Repository
public class InvalidDealDaoImpl implements InvalidDealDao {

    @PersistenceUnit
    private EntityManagerFactory emf;

    @Autowired
    public InvalidDealDaoImpl(EntityManagerFactory entityManagerFactory) {
        this.emf = entityManagerFactory;
    }

    @Override
    public void save(List<InvalidDeals> invalidDeals) {

        int count = 1;
        EntityManager em = emf.createEntityManager();

        try {
            EntityTransaction txn = em.getTransaction();
            txn.begin();

            for (InvalidDeals invalidDeal : invalidDeals) {

                em.persist(invalidDeal);

                if (count % 100 == 0) {
                    txn.commit();
                    em.clear();
                    txn.begin();
                }

                count++;
            }

            txn.commit();
        } catch (Exception e) {

        } finally {
            em.clear();
            em.close();
        }
    }

    @Override
    @Transactional
    public List<InvalidDeals> getInvalidDeals(String fileName) {
        EntityManager em = emf.createEntityManager();
        return em.createQuery("Select d FROM InvalidDeals d where d.sourceDeals.fileName=:fileName")
                .setParameter("fileName", fileName)
                .getResultList();
    }

    @Override
    @Transactional
    public long getInvalidDealCount(String fileName) {
        EntityManager em = emf.createEntityManager();
        return (Long) em.createQuery("Select count(d.id) FROM InvalidDeals d where d.sourceDeals.fileName=:fileName")
                .setParameter("fileName", fileName)
                .getSingleResult();

    }
}
