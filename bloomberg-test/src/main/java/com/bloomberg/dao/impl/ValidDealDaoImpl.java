package com.bloomberg.dao.impl;

import com.bloomberg.dao.ValidDealDao;
import com.bloomberg.entity.ValidDeals;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceUnit;
import javax.transaction.Transactional;
import java.util.List;

@Repository
public class ValidDealDaoImpl implements ValidDealDao {

    @PersistenceUnit
    private EntityManagerFactory emf;

    @Autowired
    public ValidDealDaoImpl(EntityManagerFactory entityManagerFactory) {
        this.emf = entityManagerFactory;
    }

    @Override
    public void save(List<ValidDeals> validDeals) {
        int count = 1;
        EntityManager em = emf.createEntityManager();

        try {
            EntityTransaction txn = em.getTransaction();
            txn.begin();

            for (ValidDeals validDeal : validDeals) {

                em.persist(validDeal);

                if (count % 100 == 0) {
                    txn.commit();
                    em.clear();
                    txn.begin();
                }
                count++;
            }
            txn.commit();
        } catch (Exception e) {

        } finally {
            em.clear();
            em.close();
        }
    }

    @Override
    @Transactional
    public List<ValidDeals> getValidDeals(String fileName) {
        EntityManager em = emf.createEntityManager();
        return em.createQuery("Select d FROM ValidDeals d where d.sourceDeals.fileName=:fileName")
                .setParameter("fileName", fileName)
                .getResultList();
    }

    @Override
    @Transactional
    public long getValidDealCount(String fileName) {
        EntityManager em = emf.createEntityManager();
        return (Long) em.createQuery("Select count(d.id) FROM ValidDeals d where d.sourceDeals.fileName=:fileName")
                .setParameter("fileName", fileName)
                .getSingleResult();

    }
}
