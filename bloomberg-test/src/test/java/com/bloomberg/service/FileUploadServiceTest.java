package com.bloomberg.service;


import com.bloomberg.dto.UploadedDealsFileDto;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
public class FileUploadServiceTest {

    @Mock
    private static FileUploadService fileUploadService;


    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        fileUploadService = Mockito.mock(FileUploadService.class);

        UploadedDealsFileDto uploadedDealsFileDto1 = new UploadedDealsFileDto();
        uploadedDealsFileDto1.setDealEndDate(new Date());
        uploadedDealsFileDto1.setDealStartDate(new Date());
        uploadedDealsFileDto1.setFileName("HHHHH.csv");
        uploadedDealsFileDto1.setValidDealsCount(100L);
        uploadedDealsFileDto1.setInvalidDealsCount(100L);

        UploadedDealsFileDto uploadedDealsFileDto2 = new UploadedDealsFileDto();
        uploadedDealsFileDto1.setDealEndDate(new Date());
        uploadedDealsFileDto1.setDealStartDate(new Date());
        uploadedDealsFileDto1.setFileName("bbbbb.csv");
        uploadedDealsFileDto1.setValidDealsCount(20L);
        uploadedDealsFileDto1.setInvalidDealsCount(20L);

        List<UploadedDealsFileDto> uploadedFiles = Arrays.asList(uploadedDealsFileDto1, uploadedDealsFileDto2);

        Mockito.when(fileUploadService.getUploadedFiles()).thenReturn(uploadedFiles);


    }

    @Test
    public void givenGetUploadedFileRequest_whenDisplayingFileListInIndexPage_thenReturnFileList() {
        Assert.assertTrue(fileUploadService.getUploadedFiles().size() > 0);
    }
}