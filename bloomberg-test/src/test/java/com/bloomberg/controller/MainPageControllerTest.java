package com.bloomberg.controller;

import com.bloomberg.service.DealProcessorService;
import com.bloomberg.service.FileUploadService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.io.FileInputStream;
import java.io.InputStream;

@RunWith(SpringJUnit4ClassRunner.class)
@TestPropertySource(
        locations = "classpath:application.properties")
public class MainPageControllerTest {

    private MockMvc mockMvc;

    @Mock
    private FileUploadService fileUploadService;

    @Mock
    private DealProcessorService dealProcessorService;

    @Value("${app.filepath.sample-location}")
    private String filePath;


    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders
                .standaloneSetup(new MainPageController(fileUploadService, dealProcessorService))
                .build();

    }

    @Test
    public void givenUrl_whenGetRequestOnMainController_thenRedirectToIndexPage() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.request(HttpMethod.GET, "/"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.view().name("index"))
                .andExpect(MockMvcResultMatchers.forwardedUrl("index"))
                .andExpect(MockMvcResultMatchers.status().isOk());


    }

    @Test
    public void givenUrl_whenGeneratingFile_thenFileShouldBeGenerated() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.request(HttpMethod.POST,"/generateSampleFile"))
                .andExpect(MockMvcResultMatchers.status().isOk());
        Assert.assertEquals(200, HttpStatus.OK.value());
    }

    @Test
    public void givenCsvFile_whenUploading_thenIfFileIsEmptyShouldNotBeSaved() throws Exception {
        System.out.println(filePath);
        InputStream inputStream = new FileInputStream(filePath + "deals20190304222146.csv");
        MockMultipartFile file = new MockMultipartFile("file", "", null, inputStream);
        mockMvc.perform(MockMvcRequestBuilders.fileUpload("/uploadFile").file(file))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.view().name("index"))
                .andExpect(MockMvcResultMatchers.forwardedUrl("index"))
                .andExpect(MockMvcResultMatchers.model().attributeExists("fileUploadMessage"))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void givenCsvFile_whenUploading_thenFileWithValidAndInvalidDealShouldBeSaved() throws Exception {
        InputStream inputStream = new FileInputStream(filePath + "deals20190304222146.csv");
        MockMultipartFile file = new MockMultipartFile("file", "fileName", null, inputStream);
        mockMvc.perform(MockMvcRequestBuilders.fileUpload("/uploadFile").file(file))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.view().name("index"))
                .andExpect(MockMvcResultMatchers.forwardedUrl("index"))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }
}