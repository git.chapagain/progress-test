package com.bloomberg.util;

import com.bloomberg.dto.BaseDealsDto;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class ValidationUtilTest {

    @Mock
    BaseDealsDto baseDealsDto;


    @Before
    public void init() {
        baseDealsDto = new BaseDealsDto();
        baseDealsDto.setAmount(100.00);
        baseDealsDto.setFromCurrencyIsoCode("NPR");
        baseDealsDto.setToCurrencyIsoCode("AUS");
    }

    @Test
    public void test_valid_amount() {
        assertTrue(ValidationUtil.isValidDealAmount(20.00));
    }

    @Test(expected = NumberFormatException.class)
    public void test_invalid_amount() {
        assertFalse(ValidationUtil.isValidDealAmount(new Double("fdffdf")));
    }

    @Test
    public void test_valid_currency() {
        assertTrue(ValidationUtil.isValidCountryCode("NPR"));
    }

    @Test
    public void test_invalid_currency() {
        assertFalse(ValidationUtil.isValidCountryCode("TTT"));
    }

    @Test
    public void test_valid_deal() {
        assertTrue(ValidationUtil.isDealValid(baseDealsDto));
    }

    @Test
    public void test_invalid_deal() {
        baseDealsDto.setToCurrencyIsoCode("INR");
        assertFalse(ValidationUtil.isDealValid(baseDealsDto));
    }
}