package com.bloomberg.util;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;

public class CurrencyCodeGeneratorUtilTest {

    private static final String[] VALID_CURRENCY_CODE = new String[]{"NPR", "AUS", "GBP", "CAD", "USA", "AED"};

    private static final String[] INVALID_CURRENCY_CODE = new String[]{"ZZZ", "CCC", "QQQ", "TTT", "PLZ", "KQQ"};

    @Test
    public void givenCurrencyCodeList_whenCurrencyCodesHasLengthThree_thenValidCurrencyCode() {
        Assert.assertTrue(Arrays.stream(VALID_CURRENCY_CODE).anyMatch(co -> co.length() == 3));
    }

    @Test
    public void givenCurrencyCode_whenValidCurrencyCodesListContainsCurrencyCode_thenValidCurrencyCode() {
        Assert.assertTrue(Arrays.stream(VALID_CURRENCY_CODE).anyMatch(s -> "AUS".contains(s)));
    }

    @Test
    public void givenCurrencyCode_whenValidCurrencyCodesListDoesNotContainCurrencyCode_thenInvalidCurrencyCode() {
        Assert.assertTrue(!Arrays.stream(VALID_CURRENCY_CODE).anyMatch("ppp"::contains));
    }

    @Test
    public void givenCurrencyCode_whenInvalidCurrencyCodesListContainsCurrencyCode_thenInvalidCurrencyCode() {
        Assert.assertFalse(Arrays.stream(INVALID_CURRENCY_CODE).anyMatch(s -> "AUS".contains(s)));
    }

}