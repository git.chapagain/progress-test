package com.bloomberg.util;

import org.hamcrest.core.IsNull;
import org.junit.Test;

import static org.junit.Assert.*;

public class FileWriterUtilTest {

    @Test
    public void test_given_line_no_quote() {

        String line = "6f411dec-6528-4679-be77-e954f3442e86,USA,AED,2019-03-04 22:21:46,511.4";
        String[] result = line.split(",");

        assertThat(result, IsNull.notNullValue());
        assertTrue(result.length > 1);
        assertEquals(result[0], "6f411dec-6528-4679-be77-e954f3442e86");
        assertEquals(result[1], "USA");
        assertEquals(result[2], "AED");
        assertEquals(result[3], "2019-03-04 22:21:46");
        assertEquals(result[4], "511.4");

    }


}