package com.bloomberg.util;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.UUID;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
public class UniqueIdGeneratorUtilTest {


    @Test
    public void test_structure() {

        String str = UUID.randomUUID().toString();

        assertEquals(str.charAt(8) , '-');
        assertEquals(str.charAt(13), '-');
        assertEquals(str.charAt(18), '-');
        assertEquals(str.charAt(23), '-');
        assertEquals(str.length(), 36);
        assertFalse(str.compareTo(UUID.randomUUID().toString())==0);
    }

    @Test
    public void test_parsing() {
        UUID id1 = UUID.randomUUID();
        UUID id2 = UUID.randomUUID();
        assertNotEquals(id1, id2);
        assertNotEquals(id1.hashCode(), id2.hashCode());

        UUID id4 = UUID.fromString("aAa0BBb1-CCc2-DDd3-EEe4-FFf567891234");
        assertEquals("aaa0bbb1-ccc2-ddd3-eee4-fff567891234", id4.toString());
    }

}