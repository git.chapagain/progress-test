package com.bloomberg.util;

import com.bloomberg.dto.BaseDealsDto;
import org.junit.Assert;
import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

public class FileGeneratorUtilTest {

    static SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private static final String[] VALID_CURRENCY_CODE = new String[]{"NPR", "AUS", "GBP", "CAD", "USA", "AED"};

    @Test
    public void when_getDate_thenGetWithGivenFormat() {
        String date = FileGeneratorUtil.getTimeStamp();
        Assert.assertTrue(DATE_FORMAT.format(new Date()).equalsIgnoreCase(date));
    }

    @Test
    public void whenDividedByTwo_remainderZero_thenGenerateValidData() {
        BaseDealsDto validBaseDealsData = new BaseDealsDto("eb223f10-503e-4ff0-b7d7-ebfe9b3893d3", "USA", "AUS", "2018-01-2621:46:17", 588.27);
        Assert.assertTrue(validBaseDealsData.getAmount() > 0);
        Assert.assertTrue(Arrays.stream(VALID_CURRENCY_CODE).anyMatch(s -> validBaseDealsData.getFromCurrencyIsoCode().contains(s)));
        Assert.assertTrue(Arrays.stream(VALID_CURRENCY_CODE).anyMatch(s -> validBaseDealsData.getToCurrencyIsoCode().contains(s)));

    }

    @Test
    public void whenDividedByTwo_remainderNotZero_thenGenerateInvalidData() {
        BaseDealsDto inValidBaseDealsData = new BaseDealsDto("eb223f10-503e-4ff0-b7d7-ebfe9b3893343", "PPP", "HGA", "2018-01-2621:46:17", -1234.45);
        Assert.assertTrue(inValidBaseDealsData.getAmount() < 0);
        Assert.assertTrue(!Arrays.stream(VALID_CURRENCY_CODE).anyMatch(s -> inValidBaseDealsData.getFromCurrencyIsoCode().contains(s)));
        Assert.assertTrue(!Arrays.stream(VALID_CURRENCY_CODE).anyMatch(s -> inValidBaseDealsData.getToCurrencyIsoCode().contains(s)));

    }

}