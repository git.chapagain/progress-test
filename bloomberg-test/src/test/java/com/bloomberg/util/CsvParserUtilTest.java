package com.bloomberg.util;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by tchapagai on 12/03/2018.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
@TestPropertySource("/application.properties")
public class CsvParserUtilTest {

    @Value("${app.filepath.upload-location}")
    private String path;

    @Test
    public void givenFilePath_whenFilePathIsValid_thenGetDataFromCsvFile() throws IOException {
        //file name must be specified
        String filePath = path + "\\deals20180123085833.csv";
        CsvParserUtil.getDatasFromCsvFile(filePath);
        Assert.assertTrue(true);
    }

    @Test(expected = Exception.class)
    public void givenFilePath_ifPathNotFound_thenThrowException() throws Exception {
        String inValidFilePath = path + "\\deals2018012308580000.csv";
        CsvParserUtil mockCsvParserUtil = mock(CsvParserUtil.class);
        when(mockCsvParserUtil.getDatasFromCsvFile(inValidFilePath)).thenThrow(new Exception());
    }

    @Test
    public void givenFile_whenFileExtensionIsCSV_thenFileIsValid() {
        final String fileName = "deals20188880122015189.csv";

        String filePath = path + "\\dea777ls2018012201518849.csv";
        final byte[] content = filePath.getBytes();
        MockMultipartFile mockMultipartFile = new MockMultipartFile("file", fileName, "application/octet-stream", content);

        String fileExtension = CsvParserUtil.getFileExtension(mockMultipartFile);
        Assert.assertTrue(fileExtension.equalsIgnoreCase("csv"));

    }

    @Test
    public void givenFile_whenFileExtensionIsNotCsv_thenFileIsInvalid() {
        final String fileName = "deals20188880122015189.txt";

        String filePath = path + "\\dea777ls2018012201518849.txt";
        final byte[] content = filePath.getBytes();
        MockMultipartFile mockMultipartFile = new MockMultipartFile("file", fileName, "application/octet-stream", content);

        String fileExtension = CsvParserUtil.getFileExtension(mockMultipartFile);
        Assert.assertTrue(!fileExtension.equalsIgnoreCase("csv"));

    }


}
