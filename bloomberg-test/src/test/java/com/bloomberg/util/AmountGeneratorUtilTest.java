package com.bloomberg.util;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Random;

import static org.junit.Assert.*;

public class AmountGeneratorUtilTest {

    private static final double MIN = 10;

    private static final double MAX = 1000;

    private double getValidAmountToBeChecked;

    private double getInValidAmountToBeChecked;

    @Before
    public void init() {
        getValidAmountToBeChecked = Math.round(MIN + (MAX - MIN) * new Random().nextDouble() * 100d) / 100d;

        getInValidAmountToBeChecked = Math.round(MIN + (MIN - MAX) * new Random().nextDouble() * 100d) / 100d;
    }

    @Test
    public void givenAmount_whenSmallerOrEqualToMax_thenValidAmount() {
        Assert.assertTrue(getValidAmountToBeChecked <= MAX);
    }

    @Test
    public void givenAmount_whenGreaterThanOrEqualToMin_thenValidAmount() {
        Assert.assertTrue(getValidAmountToBeChecked >= MIN);
    }

    @Test
    public void givenAmount_whenLessThanOrEqualToZero_thenInvalidAmount() {
        Assert.assertTrue(getInValidAmountToBeChecked <= 0);
    }
}