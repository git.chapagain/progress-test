package com.bloomberg.dao;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@TestPropertySource(
        locations = "classpath:application.properties")
@EnableTransactionManagement
public class SourceDealDaoTest {

    @MockBean
    SourceDealDao sourceDealDao;

    @Test
    public void testGetAllDeals(){
        assertNotNull(sourceDealDao.findAll());
    }

    @Test
    public void testGetDealsByFileName_empty(){
        assertNull(sourceDealDao.getSourceDealsByFileName("deals20190304222146"));
    }
}