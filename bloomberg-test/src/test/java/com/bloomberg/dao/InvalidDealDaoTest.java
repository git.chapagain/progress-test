package com.bloomberg.dao;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class InvalidDealDaoTest {

    @Mock
    private InvalidDealDao invalidDealDao;

    @Test
    public void givenDealsEmpty_whenSaveDeals_thenVerifyDbCall() {
        invalidDealDao.save(new ArrayList<>());
        verify(invalidDealDao).save(any(List.class));
    }

    @Test
    public void givenInvalidDeals_whenSaveDeals_thenSaveDeals() {
        invalidDealDao.save(new ArrayList<>());
        verify(invalidDealDao).save(any(List.class));
    }

}