package com.bloomberg.dao;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class ValidDealDaoTest {

    @Mock
    private ValidDealDao validDealDao;

    @Test
    public void givenDealsEmpty_whenSaveDeals_thenVerifyDbCall() {
        validDealDao.save(new ArrayList<>());
        verify(validDealDao).save(any(List.class));
    }

    @Test
    public void givenInvalidDeals_whenSaveDeals_thenSaveDeals() {
        validDealDao.save(new ArrayList<>());
        verify(validDealDao).save(any(List.class));
    }
}